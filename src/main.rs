use bevy::{
    prelude::*,
    window::{PresentMode, PrimaryWindow, WindowMode},
};
// use bevy_pixel_camera::{PixelCameraPlugin, PixelViewport, PixelZoom};

mod board;
mod piece;
use board::{board_setup, Board, DaBoard, SpriteBoard, OFFSET, SQUARE_SIZE};
use piece::{Piece, PieceKind, Position, Side};

fn main() {
    App::new()
        .add_plugins(DefaultPlugins.set(
            // ImagePlugin::default_nearest(),
            WindowPlugin {
                primary_window: Some(Window {
                    present_mode: PresentMode::AutoVsync,
                    mode: WindowMode::Windowed,
                    resolution: (SQUARE_SIZE * 8., SQUARE_SIZE * 8.).into(),
                    ..default()
                }),
                ..default()
            },
        ))
        // .add_plugins(PixelCameraPlugin)
        .insert_resource(ClearColor(Color::DARK_GRAY))
        .insert_resource(Side::White)
        .insert_resource(MousePos(None))
        .insert_resource(ClickState::ChoosePiece)
        .insert_resource(DaBoard([[None; 8]; 8]))
        .add_state::<GameState>()
        .add_systems(Startup, camera_setup)
        .add_systems(OnEnter(GameState::Playing), (board_setup, setup))
        .add_systems(
            Update,
            (
                bevy::window::close_on_esc,
                update_cursor_position,
                move_piece,
                print_board,
            ),
        )
        .run()
}
#[derive(Component)]
struct MainCamera;

#[derive(Resource)]
struct MousePos(Option<Position>);

#[derive(Clone, Copy, Default, Eq, PartialEq, Debug, Hash, States)]
enum GameState {
    #[default]
    Playing,
    Check,
    CheckMate,
    Quit,
}

#[derive(Resource, PartialEq)]
enum ClickState {
    ChoosePiece,
    SelectSquare(Piece),
}

fn setup(mut commands: Commands) {
    let a = SpriteBoard::spawn_spriteboard(&mut commands);
    commands.insert_resource(SpriteBoard(a));
}

fn camera_setup(mut commands: Commands) {
    commands.spawn((
        Camera2dBundle::default(),
        // PixelZoom::FitSize {
        //     width: 480,
        //     height: 480,
        // },
        // PixelViewport,
        MainCamera,
    ));
}

fn update_cursor_position(
    mut mouse_square: ResMut<MousePos>,
    q_window: Query<&Window, With<PrimaryWindow>>,
    q_camera: Query<(&Camera, &GlobalTransform), With<MainCamera>>,
) {
    let (camera, camera_transform) = q_camera.single();

    let mouse_pos = q_window
        .single()
        .cursor_position()
        .and_then(|cursor| camera.viewport_to_world(camera_transform, cursor))
        .map(|ray| ray.origin.truncate());
    if let Some(pos) = mouse_pos {
        let file = ((pos.x - OFFSET) / SQUARE_SIZE).round() as i8;
        let rank = ((pos.y - OFFSET) / SQUARE_SIZE).round() as i8;
        if file > 7 || rank > 7 || file < 0 || rank < 0 {
            mouse_square.0 = None;
        } else {
            mouse_square.0 = Some(Position { file, rank });
        }
    }
}

fn move_piece(
    mouse_square: Res<MousePos>,
    mut click_state: ResMut<ClickState>,
    buttons: Res<Input<MouseButton>>,
    mut commands: Commands,
    mut transform: Query<&mut Transform>,
    mut sprite: Query<&mut Sprite>,
    spriteboard: Res<SpriteBoard>,
    mut board: ResMut<DaBoard>,
    mut side: ResMut<Side>,
    asset_server: Res<AssetServer>,
) {
    if !buttons.just_pressed(MouseButton::Left) {
        return;
    }
    if let Some(pos) = mouse_square.0 {
        if let Some(piece) = board.0[pos.rank as usize][pos.file as usize] {
            if piece.side == *side {
                spriteboard.reset_color(&mut sprite);
                spriteboard.highlight(&mut sprite, piece, &board.0);
                *click_state = ClickState::SelectSquare(piece);
                return;
            }
        }

        if let ClickState::SelectSquare(mut piece) = *click_state {
            if !piece.can_move(board.0, pos, *side) {
                info!("piece not move.");
                *click_state = ClickState::ChoosePiece;
                return;
            }

            let mut b = board.0.clone();
            b[pos.rank as usize][pos.file as usize] =
                b[piece.pos.rank as usize][piece.pos.file as usize];
            b[piece.pos.rank as usize][piece.pos.file as usize] = None;
            b[pos.rank as usize][pos.file as usize].unwrap().pos = pos;
            if let Some(c) = check_for_check(b) {
                info!("You are not allowed to move into a check.");
                if c == *side {
                    *click_state = ClickState::ChoosePiece;
                    return;
                }
            }

            if let Some(a) = board.0[pos.rank as usize][pos.file as usize] {
                info!("capturing piece.");
                commands.entity(a.entity).despawn();
                board.0[piece.pos.rank as usize][piece.pos.file as usize] = None;
            }
            let mut s_despawn = true;
            if piece.kind == PieceKind::Pawn {
                let offset = match *side {
                    Side::White => 1,
                    Side::Black => -1,
                };
                // En passant
                if let Some(a) = board.0[(pos.rank - offset) as usize][pos.file as usize] {
                    if a.kind == PieceKind::Pawn && a.move_count == 1 && a.side != *side {
                        info!("capturing piece.");
                        commands.entity(a.entity).despawn();
                        board.0[piece.pos.rank as usize][piece.pos.file as usize] = None;
                    }
                }
                // Promotion
                if pos.rank == 7 {
                    piece.promote(&mut commands, *side, pos, &asset_server);
                    s_despawn = false;
                }
            }

            // Håndtering av rokade på verst mulig måte.
            if piece.kind == PieceKind::King {
                let mut offset = 0;
                let mut file = 0;
                if pos.file - piece.pos.file == 2 {
                    offset = 5;
                    file = 7;
                    if !can_castle(board.0.clone(), *side, true) {
                        return;
                    }
                }
                if pos.file - piece.pos.file == -2 {
                    offset = 3;
                    file = 0;
                    if !can_castle(board.0.clone(), *side, false) {
                        return;
                    }
                }
                if offset != 0 {
                    *transform
                        .get_mut(board.0[pos.rank as usize][file as usize].unwrap().entity)
                        .unwrap() = Transform::from_xyz(
                        OFFSET + SQUARE_SIZE * offset as f32,
                        OFFSET + pos.rank as f32 * SQUARE_SIZE,
                        1.,
                    );
                    board.0[pos.rank as usize][offset as usize] =
                        board.0[pos.rank as usize][file as usize];
                    board.0[pos.rank as usize][file as usize] = None;
                }
            }

            if s_despawn {
                *transform.get_mut(piece.entity).unwrap() = Transform::from_xyz(
                    OFFSET + SQUARE_SIZE * pos.file as f32,
                    OFFSET + SQUARE_SIZE * pos.rank as f32,
                    1.,
                );
            }

            piece.move_count += 1;
            board.0[piece.pos.rank as usize][piece.pos.file as usize] = None;
            piece.pos = pos;
            board.0[pos.rank as usize][pos.file as usize] = Some(piece);

            spriteboard.reset_color(&mut sprite);

            match *side {
                Side::White => *side = Side::Black,
                Side::Black => *side = Side::White,
            }
            *click_state = ClickState::ChoosePiece;
        }
    } else {
        info!("You have successfully esacped the matrix.");
    }
}

fn print_board(board: Res<DaBoard>, keyboard_input: Res<Input<KeyCode>>) {
    if !keyboard_input.just_pressed(KeyCode::D) {
        return;
    }
    println!("\n+---+---+---+---+---+---+---+---+");
    for i in 0..8 {
        print!("|");
        for j in 0..8 {
            if let Some(piece) = board.0[(-i + 7) as usize][j] {
                let c = match piece.kind {
                    PieceKind::Pawn => 'P',
                    PieceKind::King => 'K',
                    PieceKind::Queen => 'Q',
                    PieceKind::Bishop => 'B',
                    PieceKind::Knight => 'H',
                    PieceKind::Rook => 'R',
                };
                print!(" {} |", c);
            } else {
                print!("   |");
            }
        }
        println!("\n+---+---+---+---+---+---+---+---+");
    }
}

fn check_for_check(board: Board) -> Option<Side> {
    let mut white_king = None;
    let mut black_king = None;
    for i in 0..8 {
        for j in 0..8 {
            if let Some(piece) = board[i][j] {
                if piece.side == Side::White && piece.kind == PieceKind::King {
                    white_king = Some(piece);
                }
                if piece.side == Side::Black && piece.kind == PieceKind::King {
                    black_king = Some(piece);
                }
            }
        }
    }
    for i in 0..8 {
        for j in 0..8 {
            if let Some(piece) = board[i][j] {
                if piece.side == Side::White {
                    for m in piece.calculate_moves(board).iter() {
                        if m == &black_king.expect("f check").pos {
                            println!("check in pos: {:?}", m);
                            return Some(Side::Black);
                        }
                    }
                }
                if piece.side == Side::Black {
                    for m in piece.calculate_moves(board).iter() {
                        if m == &white_king.expect("f check").pos {
                            println!("check in pos: {:?}", m);
                            return Some(Side::White);
                        }
                    }
                }
            }
        }
    }
    None
}

fn can_castle(board: Board, side: Side, wing: bool) -> bool {
    let mut check_pos = Vec::new();
    for i in 0..8 {
        for j in 0..8 {
            if let Some(piece) = board[i][j] {
                if piece.side == side && piece.kind == PieceKind::King {
                    check_pos.push(piece.pos);
                }
            }
        }
    }
    if wing {
        let mut p = check_pos[0];
        while p.file != 8 {
            p.file += 1;
            check_pos.push(p);
        }
    } else {
        let mut p = check_pos[0];
        while p.file != 0 {
            p.file -= 1;
            check_pos.push(p);
        }
    }
    for i in 0..8 {
        for j in 0..8 {
            if let Some(piece) = board[i][j] {
                if piece.side != side {
                    for pos in &check_pos {
                        for m in &piece.calculate_moves(board) {
                            if m == pos {
                                return false;
                            }
                        }
                    }
                }
            }
        }
    }
    true
}
