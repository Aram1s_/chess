use crate::board::{OFFSET, SQUARE_SIZE};
use crate::Board;
use bevy::prelude::*;

#[derive(Clone, Copy, PartialEq)]
pub struct Piece {
    pub entity: Entity,
    pub kind: PieceKind,
    pub side: Side,
    pub pos: Position,
    pub move_count: u8,
}

#[derive(Clone, Copy, PartialEq)]
pub enum PieceKind {
    Pawn,
    King,
    Queen,
    Bishop,
    Knight,
    Rook,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Position {
    pub rank: i8,
    pub file: i8,
}

#[derive(Clone, Copy, PartialEq, Resource)]
pub enum Side {
    White,
    Black,
}

impl Piece {
    pub fn can_move(&self, board: Board, pos: Position, side: Side) -> bool {
        if self.side != side {
            info!("Wrong side.");
            return false;
        }
        let mut a = false;
        self.calculate_moves(board).iter().for_each(|e| {
            if e == &pos {
                a = true;
                return;
            }
        });
        a
    }
    pub fn calculate_moves(&self, board: Board) -> Vec<Position> {
        match self.kind {
            PieceKind::Pawn => self.pawn_moves(board),
            PieceKind::Knight => self.knight_moves(board),
            PieceKind::Bishop => self.bishop_moves(board),
            PieceKind::Rook => self.rook_moves(board),
            PieceKind::Queen => self.queen_moves(board),
            PieceKind::King => self.king_moves(board),
        }
    }
    fn pawn_moves(&self, board: Board) -> Vec<Position> {
        let mut moves = Vec::new();
        let mut test_pos = self.pos.clone();
        let offset = match self.side {
            Side::White => 1,
            Side::Black => -1,
        };
        test_pos.rank += offset;
        if board[test_pos.rank as usize][test_pos.file as usize].is_none() {
            moves.push(test_pos);
            if self.move_count == 0
                && board[(test_pos.rank + offset) as usize][test_pos.file as usize].is_none()
            {
                moves.push(Position {
                    rank: test_pos.rank + offset,
                    file: test_pos.file,
                });
            }
        }
        if self.pos.file != 7 {
            if let Some(capture_piece) = board[test_pos.rank as usize][test_pos.file as usize + 1] {
                moves.push(capture_piece.pos);
            }
            // En passant
            if self.pos.rank == 3 || self.pos.rank == 4 {
                if let Some(capture_piece) =
                    board[(test_pos.rank - offset) as usize][test_pos.file as usize + 1]
                {
                    if capture_piece.move_count == 1 {
                        moves.push(Position {
                            rank: test_pos.rank,
                            file: test_pos.file + 1,
                        });
                    }
                }
            }
        }
        if self.pos.file != 0 {
            if let Some(capture_piece) = board[test_pos.rank as usize][test_pos.file as usize - 1] {
                moves.push(capture_piece.pos);
            }
            // En passant
            if self.pos.rank == 3 || self.pos.rank == 4 {
                if let Some(capture_piece) =
                    board[(test_pos.rank - offset) as usize][test_pos.file as usize - 1]
                {
                    if capture_piece.move_count == 1 {
                        moves.push(Position {
                            rank: test_pos.rank,
                            file: test_pos.file - 1,
                        });
                    }
                }
            }
        }
        moves
            .iter()
            .filter(|p| {
                if let Some(piece) = board[p.rank as usize][p.file as usize] {
                    piece.side != self.side
                } else {
                    true
                }
            })
            .map(|p| p.to_owned())
            .collect()
    }
    fn knight_moves(&self, board: Board) -> Vec<Position> {
        [
            Position {
                rank: self.pos.rank + 2,
                file: self.pos.file + 1,
            },
            Position {
                rank: self.pos.rank + 2,
                file: self.pos.file - 1,
            },
            Position {
                rank: self.pos.rank - 2,
                file: self.pos.file + 1,
            },
            Position {
                rank: self.pos.rank - 2,
                file: self.pos.file - 1,
            },
            Position {
                rank: self.pos.rank + 1,
                file: self.pos.file + 2,
            },
            Position {
                rank: self.pos.rank + 1,
                file: self.pos.file - 2,
            },
            Position {
                rank: self.pos.rank - 1,
                file: self.pos.file + 2,
            },
            Position {
                rank: self.pos.rank - 1,
                file: self.pos.file - 2,
            },
        ]
        .iter()
        .filter(|p| p.rank <= 7 && p.rank >= 0)
        .filter(|p| p.file <= 7 && p.file >= 0)
        .filter(|p| {
            if let Some(piece) = board[p.rank as usize][p.file as usize] {
                piece.side != self.side
            } else {
                true
            }
        })
        .map(|p| p.to_owned())
        .collect()
    }
    fn bishop_moves(&self, board: Board) -> Vec<Position> {
        [
            self.laser_check(board, 1, 1),
            self.laser_check(board, 1, -1),
            self.laser_check(board, -1, 1),
            self.laser_check(board, -1, -1),
        ]
        .concat()
    }
    fn rook_moves(&self, board: Board) -> Vec<Position> {
        [
            self.laser_check(board, 1, 0),
            self.laser_check(board, 0, 1),
            self.laser_check(board, -1, 0),
            self.laser_check(board, 0, -1),
        ]
        .concat()
    }
    fn queen_moves(&self, board: Board) -> Vec<Position> {
        [
            self.laser_check(board, 1, 0),
            self.laser_check(board, 0, 1),
            self.laser_check(board, -1, 0),
            self.laser_check(board, 0, -1),
            self.laser_check(board, 1, 1),
            self.laser_check(board, 1, -1),
            self.laser_check(board, -1, 1),
            self.laser_check(board, -1, -1),
        ]
        .concat()
    }
    fn king_moves(&self, board: Board) -> Vec<Position> {
        let mut moves = Vec::new();
        if self.move_count == 0 {
            if let Some(piece) = board[0][0] {
                if piece.move_count == 0 {
                    if match self.side {
                        Side::White => {
                            board[0][1].is_none() && board[0][2].is_none() && board[0][3].is_none()
                        }
                        Side::Black => {
                            board[7][1].is_none() && board[7][2].is_none() && board[7][3].is_none()
                        }
                    } {
                        let mut pos = self.pos.clone();
                        pos.file -= 2;
                        moves.push(pos);
                    }
                    if match self.side {
                        Side::White => board[0][5].is_none() && board[0][6].is_none(),
                        Side::Black => board[7][5].is_none() && board[7][6].is_none(),
                    } {
                        let mut pos = self.pos.clone();
                        pos.file += 2;
                        moves.push(pos);
                    }
                }
            }
        }
        [
            moves,
            vec![
                Position {
                    rank: self.pos.rank + 1,
                    file: self.pos.file,
                },
                Position {
                    rank: self.pos.rank - 1,
                    file: self.pos.file,
                },
                Position {
                    rank: self.pos.rank,
                    file: self.pos.file + 1,
                },
                Position {
                    rank: self.pos.rank,
                    file: self.pos.file - 1,
                },
                Position {
                    rank: self.pos.rank + 1,
                    file: self.pos.file + 1,
                },
                Position {
                    rank: self.pos.rank + 1,
                    file: self.pos.file - 1,
                },
                Position {
                    rank: self.pos.rank - 1,
                    file: self.pos.file + 1,
                },
                Position {
                    rank: self.pos.rank - 1,
                    file: self.pos.file - 1,
                },
            ],
        ]
        .concat()
        .iter()
        .filter(|p| p.rank <= 7 && p.rank >= 0)
        .filter(|p| p.file <= 7 && p.file >= 0)
        .filter(|p| {
            if let Some(piece) = board[p.rank as usize][p.file as usize] {
                piece.side != self.side
            } else {
                true
            }
        })
        .map(|p| p.to_owned())
        .collect()
    }
    fn laser_check(&self, board: Board, a: i8, b: i8) -> Vec<Position> {
        let mut i = 1;
        let mut moves = Vec::new();
        let mut attack = false;
        let pos = self.pos;
        let side = self.side;
        while pos.rank + a * i > -1
            && pos.rank + a * i < 8
            && pos.file + b * i > -1
            && pos.file + b * i < 8
            && !attack
        {
            if let Some(piece) = board[(pos.rank + a * i) as usize][(pos.file + b * i) as usize] {
                if piece.side == side {
                    break;
                } else {
                    attack = true;
                }
            }
            moves.push(Position {
                rank: pos.rank + a * i,
                file: pos.file + b * i,
            });
            i += 1;
        }
        moves
    }
    pub fn promote(&mut self, commands: &mut Commands, side: Side, pos: Position, asset_server: &Res<AssetServer>) {
        self.kind = PieceKind::Queen;
        let name = match side {
            Side::White => "PNG/white_queen.png",
            Side::Black => "PNG/black_quen.png",
        };
        commands.entity(self.entity).despawn();
        *self = Piece {
            kind: PieceKind::Queen,
            side,
            move_count: self.move_count,
            pos: Position {
                rank: pos.rank,
                file: pos.file,
            },
            entity: commands
                .spawn(SpriteBundle {
                    transform: Transform::from_xyz(
                        SQUARE_SIZE * pos.file as f32 + OFFSET,
                        SQUARE_SIZE * pos.rank as f32 + OFFSET,
                        1.,
                    ),
                    texture: asset_server.load(name),
                    ..default()
                })
                .id(),
        };
    }
}

pub fn spawn_piece(
    commands: &mut Commands,
    asset_server: &Res<AssetServer>,
    pos: Position,
) -> Option<Piece> {
    let side = match pos.rank {
        0 | 1 => Side::White,
        7 | 6 => Side::Black,
        _ => return None,
    };
    let kind = match (pos.rank, pos.file) {
        (1, _) | (6, _) => PieceKind::Pawn,
        (_, 0) | (_, 7) => PieceKind::Rook,
        (_, 1) | (_, 6) => PieceKind::Knight,
        (_, 2) | (_, 5) => PieceKind::Bishop,
        (_, 3) => match side {
            Side::White => PieceKind::Queen,
            Side::Black => PieceKind::Queen,
        },
        (_, 4) => match side {
            Side::White => PieceKind::King,
            Side::Black => PieceKind::King,
        },

        _ => return None,
    };

    let asset_name = format!(
        "PNG/{}_{}.png",
        match side {
            Side::White => "white",
            Side::Black => "black",
        },
        match kind {
            PieceKind::Pawn => "pawn",
            PieceKind::Knight => "knight",
            PieceKind::Rook => "rook",
            PieceKind::Queen => "queen",
            PieceKind::King => "king",
            PieceKind::Bishop => "bishop",
        }
    );

    Some(Piece {
        kind,
        side,
        move_count: 0,
        pos: Position {
            rank: pos.rank,
            file: pos.file,
        },
        entity: commands
            .spawn(SpriteBundle {
                transform: Transform::from_xyz(
                    SQUARE_SIZE * pos.file as f32 + OFFSET,
                    SQUARE_SIZE * pos.rank as f32 + OFFSET,
                    1.,
                ),
                texture: asset_server.load(asset_name),
                ..default()
            })
            .id(),
    })
}
