use crate::piece::{spawn_piece, Piece, Position};
use bevy::prelude::*;

pub const SQUARE_SIZE: f32 = 60.;
pub const OFFSET: f32 = -3.5 * SQUARE_SIZE;

pub type Board = [[Option<Piece>; 8]; 8];

#[derive(Resource)]
pub struct DaBoard(pub Board);

#[derive(Resource)]
pub struct SpriteBoard(pub [[Entity; 8]; 8]);

impl SpriteBoard {
    pub fn spawn_spriteboard(commands: &mut Commands) -> [[Entity; 8]; 8] {
        let mut board = [[None; 8]; 8];
        for i in 0..8 {
            for j in 0..8 {
                let color = match (i + j) % 2 == 0 {
                    true => Color::LIME_GREEN,
                    false => Color::WHITE,
                };

                board[i][j] = Some(
                    commands
                        .spawn(SpriteBundle {
                            transform: Transform {
                                translation: Vec3::new(
                                    SQUARE_SIZE * j as f32 + OFFSET,
                                    SQUARE_SIZE * i as f32 + OFFSET,
                                    0.,
                                ),
                                rotation: Quat::from_xyzw(0., 0., 0., 0.),
                                scale: Vec3::new(SQUARE_SIZE as f32, SQUARE_SIZE as f32, 1.),
                            },
                            sprite: Sprite { color, ..default() },
                            ..default()
                        })
                        .id(),
                );
            }
        }
        board.map(|a| a.map(|b| b.unwrap()))
    }
    pub fn reset_color(&self, sprites: &mut Query<&mut Sprite>) {
        for i in 0..8 {
            for j in 0..8 {
                let color = match (i + j) % 2 == 1 {
                    true => Color::WHITE,
                    false => Color::LIME_GREEN,
                };
                sprites.get_mut(self.0[i][j]).unwrap().color = color;
            }
        }
    }
    pub fn highlight(&self, sprites: &mut Query<&mut Sprite>, piece: Piece, board: &Board) {
        sprites
            .get_mut(self.0[piece.pos.rank as usize][piece.pos.file as usize])
            .unwrap()
            .color = Color::GREEN;
        piece.calculate_moves(*board).iter().for_each(|m| {
            sprites
                .get_mut(self.0[m.rank as usize][m.file as usize])
                .unwrap()
                .color = Color::GRAY
        });
    }
}

pub fn board_setup(
    mut commands: Commands,
    mut board: ResMut<DaBoard>,
    asset_server: Res<AssetServer>,
) {
    let board = &mut board.0;
    for i in 0..8 {
        for j in 0..8 {
            board[i][j] = spawn_piece(
                &mut commands,
                &asset_server,
                Position {
                    rank: i as i8,
                    file: j as i8,
                },
            );
        }
    }
}
